#!/usr/bin/python
import os, glob, time, calendar

lup = ''.join(open(".lastupload", "r").readlines()).strip()
lupnum = float(lup)

os.chdir("build")
files = glob.glob("**/**")
for f in files:
  if lupnum < os.stat(f).st_mtime:
	#print f
	os.system("scp \"" + f + "\" bbcityco@bbcity.co.uk:www/tom/data/" + f)

os.chdir("..")
f = open(".lastupload", "w")
now = str(float(calendar.timegm(time.gmtime())))
f.write(now + "\n")
f.close()
