#!/bin/sh

cp templates/tagged tagged/$1.n3
sed -e "s/_TAG_/$1/" tagged/$1.n3 > tagged/$1.n3.new
mv tagged/$1.n3.new tagged/$1.n3

if [ `hostname` = 'carnap' ]; then
  vim tagged/$1.n3
else
  $EDITOR tagged/$1.n3
fi

if [ -d ".hg/" ]; then
  hg add tagged/$1.n3
  sed -e "/$1/d" TODO > TODO.new
  mv TODO.new TODO
  sed -e "/$1/d" tags.txt > tags.txt.new
  mv tags.txt.new tags.txt
  hg commit -m "added $1 tag"
fi

if [ -d ".git/" ]; then
  git add tagged/$1.n3
  sed -e "/$1/d" TODO > TODO.new
  mv TODO.new TODO
  sed -e "/$1/d" tags.txt > tags.txt.new
  mv tags.txt.new tags.txt
  git add tags.txt
  git commit -m "added $1 tag"
fi
