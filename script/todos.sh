#!/bin/sh

find . -not -path "./.hg/*" -not -path "script/todos.sh" -exec grep -HIin TODO {} \;
