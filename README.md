# Blog Annotations

An experimental project to add annotations to tags and posts on my Tumblr blog.

## Prerequisites

* Redland Raptor library with rapper.
  * Debian: sudo aptitude install librdf0
  * or just from http://librdf.org/raptor/
* sed
* bash shell

## License

Code components of this project are licensed under the MIT license. This is the
build.sh script, as well as any code added in the future (probably some Python
or Ruby code).

Data added to this project is public domain. If you do not wish your material
to be made available in the public domain, please do not contribute to the
project.

## How to contribute

owl:sameAs and rdfs:comment data would be greatly appreciated for tags. Take a
look at the existing n3 files to see how to format them.

If you want, you can also provide translations into other languages. Take an
existing rdfs:comment and replicate it on another line. Then replace the
contents with a translation and the language tag with the appropriate one like
@fr (for French) or @ja (for Japanese).

All Notation-3 files should be UTF-8 encoded.

## tags.txt

tags.txt is produced using this: https://gist.github.com/726013
