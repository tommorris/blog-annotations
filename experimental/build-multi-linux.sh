#!/bin/sh

# A lame attempt at concurrency.
# 
# 
#  time ./experimental/build-multi-linux.sh
# real	0m44.151s
# user	0m35.510s
# sys	0m7.292s
# 
#  time ./build.sh
# 
# real	0m41.470s
# user	0m34.462s
# sys	0m6.624s
# 
# Guess it really is kinda IO-bound.

mkdir -p build/tagged/

#processors=`grep "processors" /proc/cpuinfo | wc -l`
processors=2
TOBUILD=`ls tagged/*.n3`

if [ -d ".hg/" ]; then
  if [ -f ".lastbuild" ]; then
	LASTREV=`cat .lastbuild | tr -d '\n'`

	# see if build_html.py has changed
	if [ `hg status -n --rev $LASTREV build_html.py | wc -l` != "0" ]; then
	  # and rebuild everything if it has
	  LASTREV="0"
	fi

	TOBUILD=`hg status -n --rev $LASTREV tagged/*.n3`
  fi

  NEXTBUILD=`hg log --rev tip --template "{node}"`
fi

if [ -d ".git/" ]; then
  # TODO: add git support
  echo "git exists"
fi

for i in $TOBUILD
do
  jobsrunning=0
  while [ $jobsrunning -gt $processors ]; do
	wait
  done
  
  (jobsrunning=$[jobsrunning+1]; echo "rapper: $i"; rapper -g -q $i -o rdfxml > build/`echo $i | sed 's/\(.*\.\)n3/\1rdf/'`; jobsrunning=$[jobsrunning-1] &)
done

for i in $TOBUILD 
do
  jobsrunning=0
  while [ $jobsrunning -gt $processors ]; do
	wait
  done
  
  (jobsrunning=$[jobsrunning+1]; J=`echo $i | sed 's/\(.*\.\)n3/build\/\1rdf/'`; echo "build_html.py: $J"; python build_html.py $J > `echo $J | sed 's/\(.*\.\)rdf/\1html/'`; jobsrunning=$[jobsrunning-1] &)
done

echo $NEXTBUILD > .lastbuild
