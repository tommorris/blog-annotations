import sys
import os
import rdflib

graph = rdflib.ConjunctiveGraph()
graph.parse(sys.argv[1])

def remove_concept(str):
	return str.replace("#concept", "")

def str_representation(str):
	return remove_concept(str).replace("http://blog.tommorris.org/tagged/", "").replace("_", " ")

# dict
predicates = {
	"sameAs": "http://www.w3.org/2002/07/owl#sameAs",
	"comment": "http://www.w3.org/2000/01/rdf-schema#comment",
	"reference": "http://dbpedia.org/property/reference",
	"broader": "http://www.w3.org/2004/02/skos/core#broader",
	"narrower": "http://www.w3.org/2004/02/skos/core#narrower",
	"related": "http://www.w3.org/2004/02/skos/core#related"
}
for k, v in predicates.iteritems():
	predicates[k] = rdflib.URIRef(v)

data = {}

# find the English comment
for i in graph.subject_objects(predicates["comment"]):
	if str(i[1].language).startswith('en'):
		data["comment"] = str(i[1])

# get sameAs
for i in graph.subject_objects(predicates["sameAs"]):
	if str(i[1]).startswith("http://dbpedia.org/resource/"):
		data["wikipedia"] = str(i[1]).replace("http://dbpedia.org/resource/", "http://en.wikipedia.org/wiki/")
		data["dbpedia"] = str(i[1])
	
	if str(i[1]).startswith("http://dewey.info/class"):
		dclassnum = str(i[1]).replace("http://dewey.info/class/", "").replace("/", "")
		data["dewey"] = {"url": str(i[1]), "repr": dclassnum}



# references
for i in graph.subject_objects(predicates["reference"]):
	if str(i[1]).startswith("http://en.citizendium.org/wiki/"):
		data["citizendium"] = str(i[1])

# skos:broader
data["related"] = []
for i in graph.subject_objects(predicates["broader"]):
	if str(i[1]) not in data["related"]:
		data["related"].append(str(i[1]))
for i in graph.subject_objects(predicates["narrower"]):
	if str(i[1]) not in data["related"]:
		data["related"].append(str(i[1]))
for i in graph.subject_objects(predicates["related"]):
	if str(i[1]) not in data["related"]:
		data["related"].append(str(i[1]))
	

# now we output
output = u"<p>\n" # should now convert everything into unicode as it gets appended
if "comment" in data:
	output += """<span property="http://www.w3.org/2000/01/rdf-schema#comment" xml:lang="en" lang="en">"""
	output += data["comment"]
	output += "</span>\n"

extlinks = []
if "wikipedia" in data:
	extlinks.append("<a href=\"" + data["wikipedia"] + "\">Wikipedia</a>")
if "citizendium" in data:
	extlinks.append("<a href=\"" + data["citizendium"] + "\">Citizendium</a>")
if len(extlinks) > 0:
	output += "</p>\n<p>"
	output += '&nbsp;&nbsp;'.join(extlinks)
if "dewey" in data:
	output += "\n<br />Dewey Decimal Classification: <a href=\"" + data["dewey"]["url"] + "\">" + data["dewey"]["repr"] + "</a>\n"
if len(data["related"]) is not 0:
	output += "\n\n<br />Related tags: \n"
	for item in data["related"]:
		output += "<a href=" + remove_concept(item) + ">" + str_representation(item) + "</a>&nbsp;\n"
output += "\n</p>"

print output
