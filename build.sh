#!/bin/sh

mkdir -p build/tagged/

TOBUILD=`ls tagged/*.n3`

if [ -d ".hg/" ]; then
  if [ -f ".lastbuild" ]; then
	LASTREV=`cat .lastbuild | tr -d '\n'`

	# see if build_html.py has changed
	if [ `hg status -n --rev $LASTREV build_html.py | wc -l` != "0" ]; then
	  # and rebuild everything if it has
	  LASTREV="0"
	fi

	TOBUILD=`hg status -n --rev $LASTREV tagged/*.n3`
  fi

  NEXTBUILD=`hg log --rev tip --template "{node}"`
fi

if [ -d ".git/" ]; then
  if [ -f ".lastbuild" ]; then
    LASTREV=`cat .lastbuild | tr -d '\n'`

    #if [ `hg status -n --rev $LASTREV build_html.py | wc -l` != "0" ]; then
    #  LASTREV=`git rev-list --reverse HEAD | head -1 | tr -d '\n'`
    #fi

    TOBUILD=`git diff $LASTREV --name-only -- tagged/*.n3`
  fi

  NEXTBUILD=`git rev-list HEAD | head -n 1`
fi

for i in $TOBUILD
do
  #echo "rapper: $i"
  rapper -g -q $i -o rdfxml > build/`echo $i | sed 's/\(.*\.\)n3/\1rdf/'`
done

for i in $TOBUILD 
do
  J=`echo $i | sed 's/\(.*\.\)n3/build\/\1rdf/'`
  #echo "build_html.py: $J"
  python build_html.py $J > `echo $J | sed 's/\(.*\.\)rdf/\1html/'`
done

echo $NEXTBUILD > .lastbuild
